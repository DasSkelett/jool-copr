# Custom patches overview

## Patch 1: multiple-definition-of-init.patch

Fixes `multiple definition of '_init'` in `libxt_JOOL_SIIT.so`.

DKMS build error:

```
make[3]: Entering directory '/tmp/rpkg/jool-2-ei5toix7/jool-4.1.9/src/usr/iptables'
gcc -Wall -pedantic -std=gnu11 -O2 -I../..   -O2 -flto=auto -ffat-lto-objects -fexceptions -g -grecord-gcc-switches -pipe -Wall -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -Wp,-D_GLIBCXX_ASSERTIONS -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -fstack-protector-strong -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -m64  -mtune=generic -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection -D_INIT=libxt_JOOL_SIIT_init -fPIC -c -o libxt_JOOL_SIIT.o libxt_JOOL_SIIT.c;
gcc -shared -fPIC -Wl,-z,relro -Wl,--as-needed  -Wl,-z,now -specs=/usr/lib/rpm/redhat/redhat-hardened-ld -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -Wl,--build-id=sha1 -specs=/usr/lib/rpm/redhat/redhat-package-notes -o libxt_JOOL_SIIT.so libxt_JOOL_SIIT.o;
/usr/bin/ld: libxt_JOOL_SIIT.o (symbol from plugin): in function `iname_validate':
(.text+0x0): multiple definition of `_init'; /usr/lib/gcc/x86_64-redhat-linux/12/../../../../lib64/crti.o:(.init+0x0): first defined here
collect2: error: ld returned 1 exit status
make[3]: *** [Makefile:37: libxt_JOOL_SIIT.so] Error 1
rm libxt_JOOL_SIIT.o
make[3]: Leaving directory '/tmp/rpkg/jool-2-ei5toix7/jool-4.1.9/src/usr/iptables'
make[2]: *** [Makefile:28: all] Error 2
make[2]: Leaving directory '/tmp/rpkg/jool-2-ei5toix7/jool-4.1.9/src/usr/iptables'
make[1]: *** [Makefile:359: all-recursive] Error 1
make: *** [Makefile:429: all-recursive] Error 1
error: Bad exit status from /var/tmp/rpm-tmp.2qT44i (%build)
```

This patch is a backport of this commit: https://github.com/NICMx/Jool/commit/490ddb0933061cab3c2a7952dffc61789deed565
