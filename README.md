# Jool COPR package

This contains source code and accompanying files for the https://copr.fedorainfracloud.org/coprs/dasskelett/jool/ package.


## Notes

### Testing

For testing run:
```
rpkg local
```

### _disable_source_fetch

```
%define _disable_source_fetch 0
```
makes rpkg / rpmbuild download the jool source tarball, to avoid having to store it in this repository.
To ensure the tarball downloaded during build is not corrupted, a hash check is performed in the `%prep` phase.

https://docs.fedoraproject.org/en-US/modularity/building-modules/local/building-modules-locally/#_spec_file_configuration_optional

## Updating

1) Change the `Version:` number in `jool.spec`
2) Download the corresponding release asset and signature from https://github.com/NICMx/Jool/releases
3) Check signatures:
   ```sh
   curl -sSL https://github.com/ydahhrk.gpg | gpg --import
   gpg --verify jool-<version>.tar.gz.asc
   ```
3) Run `sha256sum jool-<version>.tar.gz` and paste the hash output into the `%define SHA256SUM0` line
4) Commit and push, a new build will be triggered automatically by the webhook configured in the GitLab repository settings
