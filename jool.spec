Name:             jool
Version:          4.1.13
Release:          1%{?dist}
Summary:          Open Source SIIT and NAT64 Translator for Linux
License:          GPL-2.0-or-later
URL:              http://jool.mx/

%define _disable_source_fetch 0
Source0:          https://github.com/NICMx/Jool/releases/download/v%{version}/%{name}-%{version}.tar.gz
Source1:          %{name}.conf
Source2:          %{name}_siit.conf
Source10:         %{name}.service.in
Source11:         %{name}@.service.in
Source12:         %{name}_siit.service.in
Source13:         %{name}_siit@.service.in
Source14:         %{name}d.service.in
Source15:         %{name}d@.service.in

%define SHA256SUM0 0c27b26f59f98d0107968cfd7b0d5f884d3a6bc297bfece7c6bdf6af16457071

BuildRequires:    dkms
BuildRequires:    kernel-devel
BuildRequires:    redhat-rpm-config
BuildRequires:    pkgconfig(libnl-genl-3.0)
BuildRequires:    pkgconfig(xtables)
BuildRequires:    systemd

Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

Requires:         dkms
Requires:         %{name}-dkms = %{version}
Requires:         kernel-devel

%description
Jool is an IP/ICMP translator; a NAT-like kernel-based utility that converts
IPv4 traffic into IPv6 and vice versa, according to a configurable IP address
translation strategy.

The `jool_siit` module implements SIIT (sometimes known as "Stateless NAT64"),
a lightweight translator that avoids storing state by keeping a deterministic
1-to-1 relationship between IPv4 addresses and their IPv6 counterparts. It can
translate addresses using the "traditional prefix" algorithm defined in RFC
6052, and/or the Explicit Address Mapping algorithm defined in RFC 7757.

The `jool` module implements Stateful NAT64, a translator that can achieve 1
(IPv4)-to-N (IPv6) relationships through a mapping strategy based on stateful
NAT. Its full address translation algorithm is defined in RFC 6146.

Both of these functions can be attached to the kernel in Netfilter hooks or as
iptables targets.

This package contains the userspace tools. The kernel modules can be found in
the jool-dkms package.


%package dkms
Summary:          Open Source SIIT and NAT64 Translator for Linux (DKMS)

Requires:         %{name} = %{version}

%description dkms
Jool is an IP/ICMP translator; a NAT-like kernel-based utility that converts
IPv4 traffic into IPv6 and vice versa, according to a configurable IP address
translation strategy.

The `jool_siit` module implements SIIT (sometimes known as "Stateless NAT64"),
a lightweight translator that avoids storing state by keeping a deterministic
1-to-1 relationship between IPv4 addresses and their IPv6 counterparts. It can
translate addresses using the "traditional prefix" algorithm defined in RFC
6052, and/or the Explicit Address Mapping algorithm defined in RFC 7757.

The `jool` module implements Stateful NAT64, a translator that can achieve 1
(IPv4)-to-N (IPv6) relationships through a mapping strategy based on stateful
NAT. Its full address translation algorithm is defined in RFC 6146.

Both of these functions can be attached to the kernel in Netfilter hooks or as
iptables targets.

This package contains the kernel modules. The jool package contains the
userspace configuration clients.


%prep
echo "%SHA256SUM0  %SOURCE0" | sha256sum -c -
%setup -q -n %{name}-%{version}

%build
%configure
%make_build

%install
%make_install

# Install a pristine version of the source but with patches applied.
%{__mkdir_p} %{buildroot}%{_usrsrc}
cp -r . %{buildroot}%{_usrsrc}/%{name}-%{version}

# Install the documentation examples.
%{__mkdir_p} %{buildroot}%{_defaultdocdir}/%{name}/examples/
install -m 0644 "%{SOURCE1}" %{buildroot}%{_defaultdocdir}/%{name}/examples/
install -m 0644 "%{SOURCE2}" %{buildroot}%{_defaultdocdir}/%{name}/examples/

# Install SystemD unit files.
%{__mkdir_p} %{buildroot}%{_unitdir}

<%{SOURCE10} >%{buildroot}%{_unitdir}/%{name}.service %{__sed} \
  -e "s|@CONFDIR@|%{_sysconfdir}/%{name}|g" \
  -e "s|@BINDIR@|%{_bindir}|g"

<%{SOURCE11} >%{buildroot}%{_unitdir}/%{name}@.service %{__sed} \
  -e "s|@CONFDIR@|%{_sysconfdir}/%{name}|g" \
  -e "s|@BINDIR@|%{_bindir}|g"

<%{SOURCE12} >%{buildroot}%{_unitdir}/%{name}_siit.service %{__sed} \
  -e "s|@CONFDIR@|%{_sysconfdir}/%{name}|g" \
  -e "s|@BINDIR@|%{_bindir}|g"

<%{SOURCE13} >%{buildroot}%{_unitdir}/%{name}_siit@.service %{__sed} \
  -e "s|@CONFDIR@|%{_sysconfdir}/%{name}|g" \
  -e "s|@BINDIR@|%{_bindir}|g"

<%{SOURCE14} >%{buildroot}%{_unitdir}/%{name}d.service %{__sed} \
  -e "s|@CONFDIR@|%{_sysconfdir}/%{name}|g" \
  -e "s|@BINDIR@|%{_bindir}|g"

<%{SOURCE15} >%{buildroot}%{_unitdir}/%{name}d@.service %{__sed} \
  -e "s|@CONFDIR@|%{_sysconfdir}/%{name}|g" \
  -e "s|@BINDIR@|%{_bindir}|g"


%post
%systemd_post %{name}.service %{name}.service

%preun
%systemd_preun %{name}.service %{name}.service

%postun
%systemd_postun_with_restart %{name}.service %{name}.service


%post dkms
dkms add -m %{name} -v %{version} -q || :

# Rebuild and make available for the currently running kernel
dkms build -m %{name} -v %{version} -q || :
dkms install -m %{name} -v %{version} -q --force || :

%preun dkms
if [ $1 == 0 ]; then
  # Remove all versions from DKMS registry
  dkms remove -m %{name} -v %{version} -q --all || :
fi


%files
%license COPYING
%doc COPYING
%doc INSTALL
%doc README.md
%{_bindir}/jool
%{_bindir}/jool_siit
%{_bindir}/joold
%{_libdir}/xtables/libxt_JOOL.so
%{_libdir}/xtables/libxt_JOOL_SIIT.so
%{_datarootdir}/bash-completion/completions/jool.bash
%{_datarootdir}/bash-completion/completions/jool_siit.bash
%{_mandir}/man8/jool.8.gz
%{_mandir}/man8/jool_siit.8.gz
%{_mandir}/man8/joold.8.gz
%{_defaultdocdir}/jool/examples/jool.conf
%{_defaultdocdir}/jool/examples/jool_siit.conf
%{_unitdir}/jool.service
%{_unitdir}/jool@.service
%{_unitdir}/jool_siit.service
%{_unitdir}/jool_siit@.service
%{_unitdir}/joold.service
%{_unitdir}/joold@.service

%files dkms
%{_usrsrc}/%{name}-%{version}

%changelog
* Sun Feb 12 2023 DasSkelett <dasskelett@gmail.com> 4.1.9-1
- Update to 4.1.9
- Fix DKMS build error "multiple definition of _init"

* Wed Sep 01 2021 DasSkelett <dasskelett@gmail.com> 4.1.5-1
- Adopted by DasSkelett
- Update to 4.1.5

* Fri Apr 03 2020 Daniel Uvehag <daniel.uvehag@gmail.com> 4.0.8-2
- Remove erroneous modprobe file.

* Fri Apr 03 2020 Daniel Uvehag <daniel.uvehag@gmail.com> 4.0.8-1
- Update to 4.0.8

* Fri Mar 27 2020 Daniel Uvehag <daniel.uvehag@gmail.com> 4.0.7-1
- Initial package
